import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
 
import { Hero }                from './hero';
import { HeroService }         from './hero.service';
 
@Component({
  selector: 'my-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: [ './heroes.component.css' ]
})
export class HeroesComponent implements OnInit {
  heroes: Hero[];
  selectedHero: Hero;
 
  constructor(
    private heroService: HeroService,
    private router: Router) { }
 
  getHeroes(): void {
    this.heroService
        .getHeroes()
        .subscribe(heroes => this.heroes = heroes);
  }
 
  add(name: string, lastName: string): void {
    name = name.trim();
    lastName = lastName.trim();
    if (!name) { return; }
    this.heroService.createHero(name, lastName)
      .subscribe(hero => {
        this.heroes.push(hero);
        this.selectedHero = null;
      });
  }
 
  delete(hero: Hero): void {
    this.heroService
        .deleteHero(hero.id)
        .then(() => {
          this.heroes = this.heroes.filter(h => h !== hero);
          if (this.selectedHero === hero) { this.selectedHero = null; }
        });
  }
 
  ngOnInit(): void {
    this.getHeroes();
  }
 
  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }
 
  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedHero.id]);
  }

  info(hero: Hero): void { //to go to details directly
    this.onSelect(hero);
    this.gotoDetail();
  }
  deleteHero(hero: Hero): void { //to delete the selected hero
    this.delete(this.selectedHero);
  }
}
