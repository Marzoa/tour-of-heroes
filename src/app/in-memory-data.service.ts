export class InMemoryDataService {
  createDb() {
    const heroes = [
      { id: 0, name: 'Zero', lastName: 'Zero' },
      { id: 1, name: 'Mr. Nice', lastName: 'Prime' },
      { id: 2, name: 'Narco', lastName: 'Hero' },
      { id: 3, name: 'Bombasto' },
      { id: 4, name: 'Celeritas' },
      { id: 5, name: 'Magneta' },
      { id: 6, name: 'RubberMan' },
      { id: 7, name: 'Dynama' },
      { id: 8, name: 'Dr IQ' },
      { id: 9, name: 'Magma' },
      { id: 10, name: 'Tornado' }
    ];
    return {heroes};
  }
}