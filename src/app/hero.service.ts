import { Injectable }    from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable }     from 'rxjs/Observable';  

 
import 'rxjs/add/operator/toPromise';
 
import { Hero } from './hero';
 
@Injectable()
export class HeroService {
 
  private headers = new Headers({'Content-Type': 'application/json'});
  private heroesUrl = 'api/heroes';  // URL to web api
 
  constructor(private http: Http) { }
 
  getHeroes(): Observable<Hero[]> {
    return this.http.get(this.heroesUrl)
               .map(response => response.json().data as Hero[])
               .catch(this.handleError);
  } 

  getHero(id: number): Observable<Hero> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get(url)
      .map(response => response.json().data as Hero)
      .catch(this.handleError);
  }
 
  deleteHero(id: number): Promise<void> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }
 
  createHero(name: string, lastName: string): Observable<Hero> {
      return this.http
        .post(this.heroesUrl, JSON.stringify({name: name, lastName: lastName}), {headers: this.headers})
        .map(res => res.json().data as Hero)
        .catch(this.handleError);    
  }
 
  updateHero(hero: Hero): Observable<Hero> {
    const url = `${this.heroesUrl}/${hero.id}`;
    return this.http
      .put(url, JSON.stringify(hero), {headers: this.headers})
      .map(() => hero)
      .catch(this.handleError);
  }
 
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}