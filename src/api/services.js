import { Pool } from 'pg'

const pool = new Pool({
    connectionString: process.env.DATABASE_URL
  })

// -- get hero--

export const getHero = async () => {
    const heroes = await pool.query('select * from heroes')
    return {
      heroes: heroes.rows
    }
}

// --get one especific product

export const getHero = async () => {
    const hero = await pool.query('select * from heroes where hero.id=$1', [heroId])
    return {
      hero: hero.rows[0]
    }
}

// --delete hero

export const deleteHero = async () => {
    const hero = await pool.query('delete from heroes where hero.id=$1', [heroId])
}

// -- create hero

export const createHero = async () => {
    const hero = await pool.query('insert into heroes (name, lastName) values($1, $2)',
    [heroName], [heroLastName])
}

// --update hero

export const updateHero = async () => {
    const hero = await pool.query('update hero name=$2, lastName=$3 where hero.id=$1)',
    [heroId], [heroName], [heroLastName])
}