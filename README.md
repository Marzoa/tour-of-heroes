# The tour of heroes

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.28.3, to deploy on heroku. You can go to (https://the-tour-of-heroes.herokuapp.com/dashboard) to check the result.

## Pre-requisites

To run in local, you need to install `Node.js` and `npm`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.


